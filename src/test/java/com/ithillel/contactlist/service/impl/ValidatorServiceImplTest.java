package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.exception.DateFormatException;
import com.ithillel.contactlist.exception.EmailFormatException;
import com.ithillel.contactlist.exception.EmptyInputException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidatorServiceImplTest {

    @Test
    void validateEmptyInputPositive() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        sut.validateEmptyInput("abc", "fieldName");
    }

    @Test
    void validateEmptyInputNegative() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        Throwable exception = assertThrows(EmptyInputException.class, () -> {
            sut.validateEmptyInput("", "fieldName");
        });
        assertEquals(exception.getMessage(), "Empty Input for fieldName field.");
    }

    @Test
    void validateEmailInputPositive() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        sut.validateEmailInput("test@gmail.com");
    }

    @Test
    void validateEmailInputNegative() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        Throwable exception = assertThrows(EmailFormatException.class, () -> {
            sut.validateEmailInput("test");
        });
        assertEquals(exception.getMessage(), "Email input test is not valid.");
    }


    @Test
    void validateDateInput() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        sut.validateDateInput("1990-10-10");
    }

    @Test
    void validateDateInputNeg() {
        ValidatorServiceImpl sut = ValidatorServiceImpl.getInstance();
        Throwable exception = assertThrows(DateFormatException.class, () -> {
            sut.validateDateInput("1990-10-101");
        });

    }

    @Test
    void validateCustomInput() {

    }
}