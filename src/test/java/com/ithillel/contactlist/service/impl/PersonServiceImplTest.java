package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.dao.PersonDao;
import com.ithillel.contactlist.dto.PersonSaveDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.model.Person;
import com.ithillel.contactlist.model.enums.Gender;
import com.ithillel.contactlist.service.PersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {

    @Mock
    private PersonDao personDao;

    private PersonService sut;

    @BeforeEach
    void setUp() {
        sut = new PersonServiceImpl(personDao);
    }

    @Test
    void save() {
        sut.save(new PersonSaveDto());
        verify(personDao, times(1)).save(isA(Person.class));
    }

    @Test
    void findAll() {
        PersonViewDto p1 = new PersonViewDto(1L, "F1", "L1");
        PersonViewDto p2 = new PersonViewDto(2L, "F2", "L2");
        PersonViewDto p3 = new PersonViewDto(3L, "F3", "L3");
        List<PersonViewDto> expected = new ArrayList<>(){{
            add(p1);
            add(p2);
            add(p3);
        }};
        List<Person> fromDb = new ArrayList<>(){{
            add(new Person(1L, "F1", "L1", Gender.MALE, LocalDate.of(1990, 10, 10), "Kyiv", "fl1@gmail.com", null));
            add(new Person(3L, "F3", "L3", Gender.MALE, LocalDate.of(1992, 12, 12), "Kyiv", "fl3@gmail.com", null));
            add(new Person(2L, "F2", "L2", Gender.MALE, LocalDate.of(1991, 11, 11), "NY", "fl2@gmail.com", null));

        }};

        when(personDao.findAll()).thenReturn(fromDb);

        List<PersonViewDto> actual = sut.findAll();

        verify(personDao, times(1)).findAll();
//        assertEquals(expected, actual);
        assertThat(actual, hasSize(3));
        assertThat(actual, hasItems(p1, p2, p3));
    }

    @Test
    void findById() {
    }

    @Test
    void update() {
    }

    @Test
    void remove() {
    }

    @Test
    void findPhonesByPersonId() {
    }

    @Test
    void savePhonesForPersonId() {
    }
}