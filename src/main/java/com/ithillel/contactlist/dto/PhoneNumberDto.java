package com.ithillel.contactlist.dto;

import com.ithillel.contactlist.model.enums.PhoneType;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class PhoneNumberDto {
    private String phoneNumber;
    private PhoneType phoneType;
}
