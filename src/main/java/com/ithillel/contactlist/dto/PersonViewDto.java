package com.ithillel.contactlist.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PersonViewDto {
    private Long id;
    private String firstName;
    private String lastName;
}
