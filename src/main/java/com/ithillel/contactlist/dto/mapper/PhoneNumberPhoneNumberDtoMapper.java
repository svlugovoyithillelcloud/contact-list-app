package com.ithillel.contactlist.dto.mapper;

import com.ithillel.contactlist.dto.PhoneNumberDto;
import com.ithillel.contactlist.model.PhoneNumber;
import org.mapstruct.Mapper;

@Mapper
public interface PhoneNumberPhoneNumberDtoMapper {

    PhoneNumber fromDto(PhoneNumberDto dto);

    PhoneNumberDto toDto(PhoneNumber phone);
}