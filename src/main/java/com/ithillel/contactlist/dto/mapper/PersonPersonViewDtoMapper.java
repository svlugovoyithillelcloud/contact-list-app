package com.ithillel.contactlist.dto.mapper;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.model.Person;
import org.mapstruct.Mapper;

@Mapper
public interface PersonPersonViewDtoMapper {

    Person fromDto(PersonViewDto dto);

    PersonViewDto toDto(Person person);
}