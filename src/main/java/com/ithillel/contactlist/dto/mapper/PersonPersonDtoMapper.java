package com.ithillel.contactlist.dto.mapper;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.model.Person;
import org.mapstruct.Mapper;

@Mapper
public interface PersonPersonDtoMapper {

    Person fromDto(PersonDto dto);

    PersonDto toDto(Person person);
}