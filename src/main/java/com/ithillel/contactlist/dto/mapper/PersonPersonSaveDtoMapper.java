package com.ithillel.contactlist.dto.mapper;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonSaveDto;
import com.ithillel.contactlist.model.Person;
import org.mapstruct.Mapper;

@Mapper
public interface PersonPersonSaveDtoMapper {

    Person fromDto(PersonSaveDto dto);

    PersonSaveDto toDto(Person person);
}