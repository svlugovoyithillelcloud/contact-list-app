package com.ithillel.contactlist.dao;

import com.ithillel.contactlist.exception.DaoOperationException;
import com.ithillel.contactlist.exception.EntityNotFoundException;
import com.ithillel.contactlist.model.Person;
import com.ithillel.contactlist.model.PhoneNumber;
import com.ithillel.contactlist.model.enums.Gender;
import com.ithillel.contactlist.model.enums.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PersonDaoImpl implements PersonDao {

    private static final Logger LOG = LoggerFactory.getLogger(PersonDaoImpl.class);

    public static final String FIND_ALL_SQL = "SELECT * FROM persons";
    public static final String SAVE_SQL =
            "INSERT INTO persons (first_name, last_name, gender, birthday, city, email) VALUES (?, ?, ?, ?, ?, ?);";
    public static final String FIND_BY_ID_SQL = "SELECT * FROM persons WHERE id = ?";
    public static final String UPDATE_SQL =
            "UPDATE persons SET first_name = ?, last_name = ?, gender = ?, birthday = ?, city = ?, email = ? WHERE id = ?";
    public static final String DELETE_SQL = "DELETE FROM persons WHERE id = ?";
    public static final String FIND_NUM_BY_ID_SQL = "SELECT * FROM phone_numbers WHERE persons_id = ?";
    public static final String SAVE_NUM_SQL = "INSERT INTO phone_numbers (phone_number, phone_type, persons_id) VALUES (?, ?, ?);";
    private DataSource dataSource;

    public PersonDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Long save(Person person) {

        LOG.info("save from dao invoked...");

        Objects.requireNonNull(person);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SAVE_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setString(3, person.getGender().toString());
            preparedStatement.setDate(4, Date.valueOf(person.getBirthday()));
            preparedStatement.setString(5, person.getCity());
            preparedStatement.setString(6, person.getEmail());
            preparedStatement.executeUpdate();

            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                long id = generatedKey.getLong("id");
                person.setId(id);
                return id;
            } else {
                throw new DaoOperationException("No Id returned after save person");
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error saving person", e);
        }
    }

    @Override
    public List<Person> findAll() {

        LOG.info("findAll from dao invoked...");

        List<Person> persons = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL_SQL);

            while (resultSet.next()) {
                Person person = new Person();
                person.setId(resultSet.getLong("id"));
                person.setFirstName(resultSet.getString("first_name"));
                person.setLastName(resultSet.getString("last_name"));
                person.setGender(Gender.valueOf(resultSet.getString("gender")));
                person.setBirthday(resultSet.getDate("birthday").toLocalDate());
                person.setCity(resultSet.getString("city"));
                person.setEmail(resultSet.getString("email"));

                persons.add(person);
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during find all persons", e);
        }
        return persons;
    }

    @Override
    public Person findById(Long id) {

        LOG.info("findById from dao invoked...");

        Objects.requireNonNull(id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Person person = new Person();
                person.setId(resultSet.getLong("id"));
                person.setFirstName(resultSet.getString("first_name"));
                person.setLastName(resultSet.getString("last_name"));
                person.setGender(Gender.valueOf(resultSet.getString("gender")));
                person.setBirthday(resultSet.getDate("birthday").toLocalDate());
                person.setCity(resultSet.getString("city"));
                person.setEmail(resultSet.getString("email"));

                return person;
            } else {
                throw new EntityNotFoundException(id);
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during find person", e);
        }
    }

    @Override
    public void update(Person person) {

        LOG.info("update from dao invoked...");

        Objects.requireNonNull(person);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setString(3, person.getGender().toString());
            preparedStatement.setDate(4, Date.valueOf(person.getBirthday()));
            preparedStatement.setString(5, person.getCity());
            preparedStatement.setString(6, person.getEmail());
            preparedStatement.setLong(7, person.getId());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new DaoOperationException("Can not update person with id " + person.getId());
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during update person", e);
        }
    }

    @Override
    public void remove(Long id) {

        LOG.info("remove from dao invoked...");

        Objects.requireNonNull(id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoOperationException("Error during delete person with id = " + id, e);
        }
    }

    @Override
    public List<PhoneNumber> findPhonesByPersonId(Long id) {

        LOG.info("findPhonesByContactId from dao invoked...");

        Objects.requireNonNull(id);

        List<PhoneNumber> phones = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_NUM_BY_ID_SQL);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setId(resultSet.getLong("id"));
                phoneNumber.setPhoneNumber(resultSet.getString("phone_number"));
                phoneNumber.setPhoneType(PhoneType.valueOf(resultSet.getString("phone_type")));

                phones.add(phoneNumber);
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during find phones", e);
        }
        return phones;
    }

    @Override
    public Long savePhoneForPersonId(PhoneNumber phone, Long id) {

        LOG.info("savePhoneForPersonId from dao invoked...");

        Objects.requireNonNull(phone);
        Objects.requireNonNull(id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SAVE_NUM_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, phone.getPhoneNumber());
            preparedStatement.setString(2, phone.getPhoneType().toString());
            preparedStatement.setLong(3, id);

            preparedStatement.executeUpdate();

            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                long generatedId = generatedKey.getLong("id");
                phone.setId(id);
                return generatedId;
            } else {
                throw new DaoOperationException("No Id returned after save phone");
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error saving phone", e);
        }
    }
}
