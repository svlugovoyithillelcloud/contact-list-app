package com.ithillel.contactlist.dao;

import com.ithillel.contactlist.model.Person;
import com.ithillel.contactlist.model.PhoneNumber;

import java.util.List;

public interface PersonDao {

    Long save(Person person);

    List<Person> findAll();

    Person findById(Long id);

    void update(Person book);

    void remove(Long id);

    List<PhoneNumber> findPhonesByPersonId(Long id);

    Long savePhoneForPersonId(PhoneNumber phone, Long id);
}
