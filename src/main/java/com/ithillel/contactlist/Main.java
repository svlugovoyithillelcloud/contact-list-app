package com.ithillel.contactlist;

import com.ithillel.contactlist.dao.PersonDao;
import com.ithillel.contactlist.dao.PersonDaoImpl;
import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonSaveDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.dto.PhoneNumberDto;
import com.ithillel.contactlist.exception.*;
import com.ithillel.contactlist.model.enums.Gender;
import com.ithillel.contactlist.model.enums.PhoneType;
import com.ithillel.contactlist.service.*;
import com.ithillel.contactlist.service.impl.*;
import com.ithillel.contactlist.util.FileReader;
import com.ithillel.contactlist.util.JdbcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private final static String TABLE_INITIALIZATION_SQL_FILE = "db/migration/table_initialization.sql";
    private final static String TABLE_POPULATION_SQL_FILE = "db/migration/table_population.sql";

    private static DataSource dataSource;
    private static PersonDao personDao;

    private static PersonService personService;
    private static ImportExportService importExportService;
    private static PrettyPrinterService printerService = new PrettyPrinterServiceImpl();
    private static ValidatorService validatorService = ValidatorServiceImpl.getInstance();
    private static EmailService emailService;

    public static void main(String[] args) {

        String db_name = null;

        try {
            db_name = args[0];
        } catch (ArrayIndexOutOfBoundsException ex) {
            printerService.print("=== You are not pass db name, will be used default 'contacts_db' ===");
        }

        initDatasource(db_name != null ? db_name : "contacts_db");
        initTablesInDB();
        populateTablesInDB();

        initDao();
        initSvc();

        Scanner sc = new Scanner(System.in);

        showHelp();

        while (true) {
            printerService.print("-> ");

            String s = sc.nextLine();
            switch (s) {
                case "1":
                    LOG.info("User entered {}", s);
                    showAllContacts();
                    break;
                case "2":
                    LOG.info("User entered {}", s);
                    showContactDetails(sc);
                    break;
                case "3":
                    LOG.info("User entered {}", s);
                    addNewContact(sc);
                    break;
                case "4":
                    LOG.info("User entered {}", s);
                    updateContact(sc);
                    break;
                case "5":
                    LOG.info("User entered {}", s);
                    removeContact(sc);
                    break;
                case "6":
                    LOG.info("User entered {}", s);
                    showContactPhones(sc);
                    break;
                case "7":
                    LOG.info("User entered {}", s);
                    importContactsFromFile(sc);
                    break;
                case "8":
                    LOG.info("User entered {}", s);
                    exportContactsToFile(sc);
                    break;
                case "9":
                    LOG.info("User entered {}", s);
                    enableDirectoryMonitoring(sc);
                    break;
                case "10":
                    LOG.info("User entered {}", s);
                    sendEmailWithAllContacts(sc);
                    break;

                case "11":
                    LOG.info("User entered {}", s);
                    showHelp();
                    break;
                case "12":
                    LOG.info("User entered {}", s);
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
    }


    private static void showHelp() {
        printerService.printHelp();
    }

    private static void showAllContacts() {
        List<PersonViewDto> viewDtos = personService.findAll();
        printerService.prettyPrintPersonList(viewDtos);
    }

    private static void showContactDetails(Scanner sc) {
        boolean flag = true;
        PersonDto personDto = null;
        while (flag) {
            printerService.print("Введите ID контакта: ");
            String id = sc.nextLine();
            try {
                if (id.isBlank()) {
                    throw new EmptyInputException("id");
                }
                personDto = personService.findById(Long.valueOf(id));
                flag = false;
            } catch (EmptyInputException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Поле id не должно быть пустым.");
            } catch (EntityNotFoundException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Контакта с id = " + id + " не существует в базе данных.");
            } catch (NumberFormatException ex) {
                LOG.error(ex.toString());
                printerService.print("Формат id = " + id + " не валидный, введите число.");
            }
        }
        printerService.prettyPrintPerson(personDto);
    }

    private static void addNewContact(Scanner sc) {

        boolean nameInputFlag = true;
        String firstName = null;
        while (nameInputFlag) {
            printerService.print("Введите имя: ");
            firstName = sc.nextLine();
            try {
                validatorService.validateEmptyInput(firstName, "firstName");
                nameInputFlag = false;
            } catch (EmptyInputException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Поле firstName не должно быть пустым.");
            }
        }

        printerService.print("Введите фамилию: ");
        String lastName = sc.nextLine();

        boolean genderInputFlag = true;
        String gender_value = null;
        Gender gender = null;
        while (genderInputFlag) {
            printerService.print("Введите пол - 1 (мужчина), 2 (женщина): ");
            gender_value = sc.nextLine();
            try {
                validatorService.validateCustomInput(gender_value, new String[]{"1", "2"});
                gender = gender_value.equals("1") ? Gender.MALE : Gender.FEMALE;
                genderInputFlag = false;
            } catch (NotSupportedInputException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Bведенное значение не поддерживается.");
            }
        }

        boolean dateInputFlag = true;
        String birthday_value = null;
        LocalDate birthday = null;
        while (dateInputFlag) {
            printerService.print("Введите дату рождения ГГГГ:ММ:ДД (например, 1990-10-25): ");
            birthday_value = sc.nextLine();
            try {
                validatorService.validateEmptyInput(birthday_value, "birthday");
                validatorService.validateDateInput(birthday_value);
                birthday = LocalDate.parse(birthday_value);
                dateInputFlag = false;
            } catch (EmptyInputException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Поле birthday не должно быть пустым.");
            } catch (DateFormatException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Дата введена в неправильном формате.");
            }
        }

        printerService.print("Введите город: ");
        String city = sc.nextLine();

        boolean emailInputFlag = true;
        String email = null;
        while (emailInputFlag) {
            printerService.print("Введите email: ");
            email = sc.nextLine();
            try {
                validatorService.validateEmptyInput(email, "email");
                validatorService.validateEmailInput(email);
                emailInputFlag = false;
            } catch (EmptyInputException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Поле email не должно быть пустым.");
            } catch (EmailFormatException ex) {
                LOG.error(ex.getMessage());
                printerService.print("Email введен в неправильном формате.");
            }
        }

        printerService.print("Введите домашний номер телефона: ");
        String home = sc.nextLine();
        printerService.print("Введите рабочий номер телефона: ");
        String work = sc.nextLine();

        PersonSaveDto personSaveDto = PersonSaveDto.builder()
                .firstName(firstName)
                .lastName(lastName)
                .gender(gender)
                .birthday(birthday)
                .city(city)
                .email(email)
                .build();

        Long id = personService.save(personSaveDto);

        List<PhoneNumberDto> phoneDtos = new ArrayList<>();
        if (home != null && !home.isBlank()) {
            phoneDtos.add(new PhoneNumberDto(home, PhoneType.HOME));
        }
        if (work != null && !work.isBlank()) {
            phoneDtos.add(new PhoneNumberDto(work, PhoneType.WORK));
        }
        List<Long> ids = personService.savePhonesForPersonId(phoneDtos, id);
        LOG.info("Phones successful saved for person id = {} with ids = {}", id, ids);

        printerService.print("Контакт успешно сохранен, ID = " + id);
    }


    private static void updateContact(Scanner sc) {
        printerService.print("Sorry, this functional not implemented yet...");
    }

    private static void removeContact(Scanner sc) {
        printerService.print("Sorry, this functional not implemented yet...");
    }

    private static void showContactPhones(Scanner sc) {
        printerService.print("Введите ID контакта: ");
        String id = sc.nextLine();
        List<PhoneNumberDto> phoneNumberDtos = personService.findPhonesByPersonId(Long.valueOf(id));
        PersonDto personDto = personService.findById(Long.valueOf(id));
        printerService.print(personDto.getFirstName() + " " + personDto.getLastName() + ":");
        printerService.prettyPrintPhoneList(phoneNumberDtos);
    }

    private static void importContactsFromFile(Scanner sc) {
        printerService.print("Введите путь к файлу (например, /Users/serhiiluhovyi/Movies/import.txt): ");
        String fileName = sc.nextLine();

        List<Long> ids = importExportService.importFromFile(fileName);

        printerService.print("Контакты успешно импортированы и сохранены, IDs = " + ids);
    }

    private static void exportContactsToFile(Scanner sc) {
        printerService.print("Введите путь к файлу (например, /Users/serhiiluhovyi/Movies/export.txt): ");
        String fileName = sc.nextLine();

        importExportService.exportToFile(fileName);

        printerService.print("Контакты успешно экспортированы в файл " + fileName);
    }

    private static void enableDirectoryMonitoring(Scanner sc) {
        printerService.print("Sorry, this functional not implemented yet...");
    }


    private static void sendEmailWithAllContacts(Scanner sc) {
        printerService.print("Введите email получателя (например, svlugovoy.ithillel@gmail.com): ");
        String email = sc.nextLine();

        List<PersonViewDto> viewDtos = personService.findAll();
        importExportService.exportToFile("/Users/serhiiluhovyi/Movies/export.txt");

        try {
            emailService.sendEmailTo(email, viewDtos, "/Users/serhiiluhovyi/Movies/export.txt");
            printerService.print("Контакты успешно отправлены на " + email);
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }


    private static void initDatasource(String dbName) {
        dataSource = JdbcUtil.createPostgresDataSource(
                "jdbc:postgresql://localhost:5432/" + dbName, "postgres", "Password1");
    }

    private static void initTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_INITIALIZATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables init.", e);
        }
    }

    private static void populateTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_POPULATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables population.", e);
        }
    }

    private static void initDao() {
        personDao = new PersonDaoImpl(dataSource);
    }

    private static void initSvc() {
        personService = new PersonServiceImpl(personDao);
        importExportService = new ImportExportServiceImpl(personDao);
        emailService = new EmailServiceImpl();
    }
}
