package com.ithillel.contactlist.model.enums;

public enum Gender {
    MALE, FEMALE
}
