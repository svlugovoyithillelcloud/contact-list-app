package com.ithillel.contactlist.model;

import com.ithillel.contactlist.model.enums.PhoneType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder
public class PhoneNumber {
    private Long id;
    private String phoneNumber;
    private PhoneType phoneType;
}
