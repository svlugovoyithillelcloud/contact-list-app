package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

//https://myaccount.google.com/lesssecureapps
//https://crunchify.com/java-mailapi-example-send-an-email-via-gmail-smtp/

public class EmailServiceImpl implements EmailService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

    private static Properties mailServerProperties;
    private static Session mailSession;
    private static MimeMessage mailMessage;

    @Override
    public void sendEmailTo(String email, List<PersonViewDto> dtos, String file) throws MessagingException, IOException {

        // Step1
        LOG.info("1st ===> setup Mail Server Properties..");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        LOG.info("Mail Server Properties have been setup successfully..");

        // Step2
        LOG.info("\n\n 2nd ===> get Mail Session..");
        mailSession = Session.getDefaultInstance(mailServerProperties, null);
        mailMessage = new MimeMessage(mailSession);
        mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
        mailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("svlugovoy@gmail.com"));
        mailMessage.setSubject("[Important] Dump of All Contacts from cl-app");

        StringBuilder sb = new StringBuilder();
        sb.append("Hello, this is contacts from cl-app.<br><br>");
        dtos.forEach(dto -> {
            sb.append(dto + "<br>");
        });
        String emailBody = sb.toString() + "<br><br>Best Regards, <br>Serhii Luhovyi";

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(emailBody, "text/html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
        attachmentBodyPart.attachFile(new File(file));
        multipart.addBodyPart(attachmentBodyPart);

        mailMessage.setContent(multipart);
        LOG.info("Mail Session has been created successfully..");

        // Step3
        LOG.info("\n\n 3rd ===> Get Session and Send mail");
        Transport transport = mailSession.getTransport("smtp");

        // Enter your correct gmail UserID and Password
        // if you have 2FA enabled then provide App Specific Password
        transport.connect("smtp.gmail.com", "svlugovoy.ithillel@gmail.com", "очень-большой-секрет");
        transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
        transport.close();
    }
}
