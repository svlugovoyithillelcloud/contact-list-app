package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.exception.DateFormatException;
import com.ithillel.contactlist.exception.EmailFormatException;
import com.ithillel.contactlist.exception.EmptyInputException;
import com.ithillel.contactlist.exception.NotSupportedInputException;
import com.ithillel.contactlist.service.ValidatorService;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

public class ValidatorServiceImpl implements ValidatorService {

    private static ValidatorServiceImpl instance;

    private ValidatorServiceImpl() {
    }

    public static ValidatorServiceImpl getInstance() {
        if (instance == null) {
            instance = new ValidatorServiceImpl();
        }
        return instance;
    }

    @Override
    public void validateEmptyInput(String input, String field) {
        if (input.isBlank()) {
            throw new EmptyInputException(field);
        }
    }

    @Override
    public void validateEmailInput(String input) {
        if (!input.contains("@")) {
            throw new EmailFormatException(input);
        }
    }

    @Override
    public void validateDateInput(String input) {
        try {
            LocalDate.parse(input);
        } catch (DateTimeParseException ex) {
            throw new DateFormatException(input);
        }
    }

    @Override
    public void validateCustomInput(String input, String[] validVariants) {
        boolean valid = Arrays.stream(validVariants).anyMatch(input::equals);
        if (!valid) {
            throw new NotSupportedInputException(input, validVariants);
        }
    }
}
