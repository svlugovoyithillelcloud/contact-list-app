package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.dao.PersonDao;
import com.ithillel.contactlist.dao.PersonDaoImpl;
import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonSaveDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.dto.PhoneNumberDto;
import com.ithillel.contactlist.dto.mapper.PersonPersonDtoMapper;
import com.ithillel.contactlist.dto.mapper.PersonPersonSaveDtoMapper;
import com.ithillel.contactlist.dto.mapper.PersonPersonViewDtoMapper;
import com.ithillel.contactlist.dto.mapper.PhoneNumberPhoneNumberDtoMapper;
import com.ithillel.contactlist.model.Person;
import com.ithillel.contactlist.model.PhoneNumber;
import com.ithillel.contactlist.service.PersonService;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

public class PersonServiceImpl implements PersonService {

    private static final Logger LOG = LoggerFactory.getLogger(PersonServiceImpl.class);

    private PersonDao personDao;

    private PersonPersonDtoMapper personPersonDtoMapper = Mappers.getMapper(PersonPersonDtoMapper.class);
    private PersonPersonSaveDtoMapper personPersonSaveDtoMapper = Mappers.getMapper(PersonPersonSaveDtoMapper.class);
    private PersonPersonViewDtoMapper personPersonViewDtoMapper = Mappers.getMapper(PersonPersonViewDtoMapper.class);
    private PhoneNumberPhoneNumberDtoMapper phoneNumberPhoneNumberDtoMapper = Mappers.getMapper(PhoneNumberPhoneNumberDtoMapper.class);


    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public Long save(PersonSaveDto dto) {

        LOG.info("Save person started...");
        LOG.info("Passed personDto - {}", dto);

        Person person = personPersonSaveDtoMapper.fromDto(dto);

        return personDao.save(person);
    }

    @Override
    public List<PersonViewDto> findAll() {

        LOG.info("Get all persons started...");

        List<Person> personList = personDao.findAll();

        LOG.debug("Received persons from db - {}", personList); //todo show debug level output

        List<PersonViewDto> personViewDtos = personList.stream()
                .map(p -> personPersonViewDtoMapper.toDto(p))
                .collect(Collectors.toList());

        return personViewDtos;

    }

    @Override
    public PersonDto findById(Long id) {

        LOG.info("Started search person with id = {}", id);

        Person person = personDao.findById(id);
        PersonDto personDto = personPersonDtoMapper.toDto(person);

        return personDto;
    }

    @Override
    public void update(PersonDto dto) {

        LOG.info("Started update person - {}", dto);

        Person person = personPersonDtoMapper.fromDto(dto);

        personDao.update(person);
    }

    @Override
    public void remove(Long id) {

        LOG.info("Started remove person with id = {}", id);

        personDao.remove(id);
    }

    @Override
    public List<PhoneNumberDto> findPhonesByPersonId(Long id) {

        LOG.info("Started search phones for person with id = {}", id);

        List<PhoneNumber> phones = personDao.findPhonesByPersonId(id);

        List<PhoneNumberDto> phonesDtos = phones.stream()
                .map(n -> phoneNumberPhoneNumberDtoMapper.toDto(n))
                .collect(Collectors.toList());

        return phonesDtos;
    }

    @Override
    public List<Long> savePhonesForPersonId(List<PhoneNumberDto> dtos, Long id) {
        List<PhoneNumber> phones = dtos.stream()
                .map(dto -> phoneNumberPhoneNumberDtoMapper.fromDto(dto))
                .collect(Collectors.toList());

        List<Long> ids = phones.stream()
                .map(p -> personDao.savePhoneForPersonId(p, id))
                .collect(Collectors.toList());

        return ids;
    }
}
