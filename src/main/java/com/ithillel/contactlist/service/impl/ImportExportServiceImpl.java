package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.dao.PersonDao;
import com.ithillel.contactlist.dao.PersonDaoImpl;
import com.ithillel.contactlist.model.Person;
import com.ithillel.contactlist.model.PhoneNumber;
import com.ithillel.contactlist.model.enums.Gender;
import com.ithillel.contactlist.model.enums.PhoneType;
import com.ithillel.contactlist.service.ImportExportService;
import com.ithillel.contactlist.util.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImportExportServiceImpl implements ImportExportService {

    private static final Logger LOG = LoggerFactory.getLogger(ImportExportServiceImpl.class);

    private PersonDao personDao;

    public ImportExportServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public List<Long> importFromFile(String fileName) {

        LOG.info("Import persons from file {} started...", fileName);

        Stream<String> stringStream = FileReader.readLinesFromFile(fileName);
        List<Person> personList = stringStream
                .map(str -> {
                    String[] values = new String[0];
                    try {
                        values = str.split(getDelimiter());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //[Leonid, Lion, MALE, 1980-05-05, Kyiv, lion@gmail.com, , 0975559922]
                    Person person = new Person();
                    person.setFirstName(values[0]);
                    person.setLastName(values[1]);
                    person.setGender(Gender.valueOf(values[2]));
                    person.setBirthday(LocalDate.parse(values[3]));
                    person.setCity(values[4]);
                    person.setEmail(values[5]);

                    Long id = personDao.save(person);
                    person.setId(id);

                    if (values[6] != null && !values[6].isBlank()) {
                        personDao.savePhoneForPersonId(new PhoneNumber(null, values[6], PhoneType.WORK), id);
                        LOG.debug("Saved phone {} for person with id = {}", values[6], id);
                    }
                    if (values[7] != null && !values[7].isBlank()) {
                        personDao.savePhoneForPersonId(new PhoneNumber(null, values[7], PhoneType.HOME), id);
                        LOG.debug("Saved phone {} for person with id = {}", values[7], id);
                    }

                    return person;
                })
                .collect(Collectors.toList());

        return personList.stream()
                .map(p -> p.getId())
                .collect(Collectors.toList());
    }

    @Override
    public void exportToFile(String fileName) {

        LOG.info("Export persons to file {} started...", fileName);

        List<Person> personList = personDao.findAll();

        try (PrintWriter pw = new PrintWriter(new FileWriter(fileName))) {

            personList.forEach(
                    p -> {
                        List<PhoneNumber> phones = personDao.findPhonesByPersonId(p.getId());
                        Map<PhoneType, List<String>> phonesMap = phones.stream()
                                .collect(Collectors.groupingBy(
                                        PhoneNumber::getPhoneType,
                                        Collectors.mapping(PhoneNumber::getPhoneNumber, Collectors.toList())
                                ));

                        StringBuilder sb = new StringBuilder();

                        String delimiter = null;
                        try {
                            delimiter = getDelimiter();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        sb.append(p.getFirstName())
                                .append(delimiter)
                                .append(p.getLastName())
                                .append(delimiter)
                                .append(p.getGender().toString())
                                .append(delimiter)
                                .append(p.getBirthday())
                                .append(delimiter)
                                .append(p.getCity())
                                .append(delimiter)
                                .append(p.getEmail())
                                .append(delimiter)
                                .append(phonesMap.get(PhoneType.WORK) != null ?
                                        phonesMap.get(PhoneType.WORK).get(0) :
                                        "")
                                .append(delimiter)
                                .append(phonesMap.get(PhoneType.HOME) != null ?
                                        phonesMap.get(PhoneType.HOME).get(0) :
                                        "");
                        pw.println(sb);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getDelimiter() throws Exception {
        Properties p = new Properties();
        p.load(new java.io.FileReader("src/main/resources/application.properties"));
        return p.getProperty("file.delimiter");
    }
}
