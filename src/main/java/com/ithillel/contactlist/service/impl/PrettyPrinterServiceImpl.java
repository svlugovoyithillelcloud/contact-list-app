package com.ithillel.contactlist.service.impl;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.dto.PhoneNumberDto;
import com.ithillel.contactlist.model.enums.PhoneType;
import com.ithillel.contactlist.service.PrettyPrinterService;

import java.util.List;

public class PrettyPrinterServiceImpl implements PrettyPrinterService {

    @Override
    public void print(String str) {
        System.out.println(str);
    }

    @Override
    public void printHelp() {
        System.out.println("1) Посмотреть все контакты - введите 1");
        System.out.println("2) Показать детали - введите 2 (дальше вас попросят ввести id контакта)");
        System.out.println("3) Добавить новый контакт - введите 3 (дальше вас попросят ввести данные)");
        System.out.println("4) Изменить существующий контакт - введите 4 (дальше вас попросят ввести данные)");
        System.out.println("5) Удалить существующий контакт - введите 5 (ввод данных, подтверждение удаления)");
        System.out.println("6) Показать телефоны контакта - введите 6 (дальше вас попросят ввести id контакта)");
        System.out.println("7) Импрортировать контакты из файла - введите 7 (дальше вас попросят ввести путь к файлу)");
        System.out.println("8) Экспортировать контакты из базы в файл - введите 8 (дальше вас попросят ввести путь к файлу)");
        System.out.println("9) Включить поддержку импорта контактов из сторонней директории - //not implemented yet");
        System.out.println("10) Отправить контакты из базы на почту - введите 10 (дальше вас попросят ввести email получателя)");
        System.out.println("Показать список возможных действий (help) - введите 11");
        System.out.println("Завершить программу - введите 12");
    }

    @Override
    public void prettyPrintPersonList(List<PersonViewDto> dtos) {
        dtos.forEach(dto -> {
            System.out.println(dto.getFirstName() + " " + dto.getLastName() + ", id = " + dto.getId());
        });
    }

    @Override
    public void prettyPrintPerson(PersonDto dto) {
        System.out.println(dto.getFirstName() + " " + dto.getLastName() + ", " + dto.getGender() + ", city = " + dto.getCity() +
                "\n" + dto.getEmail() +
                "\nДень Рождения: " + dto.getBirthday());
    }

    @Override
    public void prettyPrintPhoneList(List<PhoneNumberDto> dtos) {
        dtos.forEach(dto -> {
            System.out.println(
                    (dto.getPhoneType().equals(PhoneType.HOME)
                            ? "Домашний телефон: "
                            : "Рабочий телефон: ") + dto.getPhoneNumber());
        });
    }
}
