package com.ithillel.contactlist.service;

import java.util.List;

public interface ImportExportService {

    List<Long> importFromFile(String fileName);

    void exportToFile(String fileName);
}
