package com.ithillel.contactlist.service;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.dto.PhoneNumberDto;

import java.util.List;

public interface PrettyPrinterService {

    void print(String str);

    void printHelp();

    void prettyPrintPersonList(List<PersonViewDto> dtos);

    void prettyPrintPerson(PersonDto dto);

    void prettyPrintPhoneList(List<PhoneNumberDto> dtos);

}
