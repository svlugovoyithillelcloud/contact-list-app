package com.ithillel.contactlist.service;

import com.ithillel.contactlist.dto.PersonDto;
import com.ithillel.contactlist.dto.PersonSaveDto;
import com.ithillel.contactlist.dto.PersonViewDto;
import com.ithillel.contactlist.dto.PhoneNumberDto;

import java.util.List;

public interface PersonService {

    Long save(PersonSaveDto person);

    List<PersonViewDto> findAll();

    PersonDto findById(Long id);

    void update(PersonDto person);

    void remove(Long id);

    List<PhoneNumberDto> findPhonesByPersonId(Long id);

    List<Long> savePhonesForPersonId(List<PhoneNumberDto> dtos, Long id);

}
