package com.ithillel.contactlist.service;

import com.ithillel.contactlist.dto.PersonViewDto;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface EmailService {

    void sendEmailTo(String email, List<PersonViewDto> dtos, String file) throws MessagingException, IOException;

}
