package com.ithillel.contactlist.service;

public interface ValidatorService {

    void validateEmptyInput(String input, String field);

    void validateEmailInput(String input);

    void validateDateInput(String input);

    void validateCustomInput(String input, String[] validVariants);

}
