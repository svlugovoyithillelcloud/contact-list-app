package com.ithillel.contactlist.exception;

public class EmptyInputException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Empty Input for %s field.";

    public EmptyInputException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
