package com.ithillel.contactlist.exception;

public class EmailFormatException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Email input %s is not valid.";

    public EmailFormatException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
