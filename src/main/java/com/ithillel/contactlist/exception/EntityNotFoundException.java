package com.ithillel.contactlist.exception;

public class EntityNotFoundException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Entity with id = %d not found in database.";

    public EntityNotFoundException(Long value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
