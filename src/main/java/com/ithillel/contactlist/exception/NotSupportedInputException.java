package com.ithillel.contactlist.exception;

import java.util.Arrays;

public class NotSupportedInputException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Input %s not supported. Valid variants is %s";

    public NotSupportedInputException(String value, String[] validVariants) {
        super(String.format(MESSAGE_TEMPLATE, value, Arrays.toString(validVariants)));
    }
}
