package com.ithillel.contactlist.exception;

public class DateFormatException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Date input %s is not valid.";

    public DateFormatException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
