
DROP TABLE IF EXISTS persons CASCADE;
DROP TABLE IF EXISTS phone_numbers CASCADE;

CREATE TABLE persons (
  id SERIAL,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  gender VARCHAR(6) NOT NULL,
  birthday DATE NOT NULL,
  city VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,

  CONSTRAINT persons_PK PRIMARY KEY (id),
  CONSTRAINT persons_email_AK UNIQUE (email)
);

CREATE TABLE phone_numbers (
  id SERIAL,
  phone_number VARCHAR(20) NOT NULL,
  phone_type VARCHAR(10) NOT NULL,
  persons_id BIGINT NOT NULL,

  CONSTRAINT phone_numbers_PK PRIMARY KEY (id),
  CONSTRAINT phone_numbers_phone_number_AK UNIQUE (phone_number),
  CONSTRAINT phone_numbers_persons_FK FOREIGN KEY (persons_id) REFERENCES persons (id)
);



