INSERT INTO persons (first_name, last_name, gender, birthday, city, email)
    VALUES ('Ivan', 'Ivanov', 'MALE', '2000-10-10', 'Kyiv', 'ivan@gmail.com'),
            ('Petr', 'Petrov', 'MALE', '1990-06-10', 'Kyiv', 'petr@gmail.com'),
            ('Anna', 'Ivanova', 'FEMALE', '1995-08-08', 'Dnepr', 'anna@gmail.com'),
            ('Sveta', 'Kuk', 'FEMALE', '1980-01-01', 'Lviv', 'sveta@gmail.com'),
            ('Diana', 'Svetlova', 'FEMALE', '1985-10-09', 'Kyiv', 'diana@gmail.com');

INSERT INTO phone_numbers (phone_number, phone_type, persons_id)
    VALUES ('123456789', 'HOME', (select id from persons where email='ivan@gmail.com')),
            ('223456789', 'WORK', (select id from persons where email='ivan@gmail.com')),
            ('323456789', 'HOME', (select id from persons where email='petr@gmail.com')),
            ('423456789', 'WORK', (select id from persons where email='petr@gmail.com')),
            ('443456789', 'HOME', (select id from persons where email='anna@gmail.com')),
            ('523456789', 'WORK', (select id from persons where email='anna@gmail.com')),
            ('623456789', 'HOME', (select id from persons where email='sveta@gmail.com')),
            ('723456789', 'WORK', (select id from persons where email='sveta@gmail.com')),
            ('823456789', 'HOME', (select id from persons where email='diana@gmail.com')),
            ('923456789', 'WORK', (select id from persons where email='diana@gmail.com'));

